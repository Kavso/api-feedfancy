import django_filters.rest_framework as drf_filters
from rest_framework import status, filters
from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response
from core.filters import PostFilter
from core.serializers import SpiderSerializer, PublicPostSerializer
from core.models import Post


class PublicPostListView(ListAPIView):
    serializer_class = PublicPostSerializer
    filter_class = PostFilter
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, drf_filters.DjangoFilterBackend)
    ordering_fields = ('id', 'slug', 'title', 'createdAt', 'updatedAt')
    search_fields = ('id', 'title', 'summary', 'content', 'taxonomies__name')
    queryset = Post.objects.all()


class SpiderView(GenericAPIView):
    serializer_class = SpiderSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.to_representation(request.data), status=status.HTTP_200_OK)
