import requests
from django.template.defaultfilters import slugify
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed
from bs4 import BeautifulSoup
from core.models import SourceUrl, Post, Taxonomy, Source


class PublicTaxonomySerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomy
        fields = ['slug', 'name']


class PublicSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ['name', 'url']


class PublicPostSerializer(serializers.ModelSerializer):
    taxonomies = PublicTaxonomySerializer(many=True, read_only=True)
    source = PublicSourceSerializer(many=False, read_only=True)

    class Meta:
        model = Post
        fields = ['id', 'slug', 'title', 'content', 'summary', 'link', 'imgUrl', 'taxonomies', 'source']

    def create(self, validated_data):
        raise MethodNotAllowed('Solicitud no valida')

    def update(self, instance, validated_data):
        raise MethodNotAllowed('Solicitud no valida')


class SpiderSerializer(serializers.Serializer):

    def to_representation(self, instance):
        response = []
        # shows only where isActive is True
        uri = SourceUrl.objects.filter(taxonomies__slug='politica', isActive=True).order_by('?')
        for u in uri:
            r = requests.get(u.url)
            if r.status_code != 200:
                continue
            soup = BeautifulSoup(r.text, 'html.parser')
            # get data by class main from source url
            links = soup.find_all(True, {'class': [u.metadata['parent']]}, limit=u.metadata['size'], recursive=True)
            # iter result find
            for l in links:
                post = {}
                # Apply rules for source url
                if not u.metadata:
                    continue
                for key, value in u.metadata.items():
                    # Exclude rules parent and size
                    if key != 'parent' and key != 'size':
                        # Extracting data by rules and search
                        for a in l.find_all(value[0]):
                            # Build post by rules keys
                            if value[1] == 'get':
                                post[key] = a.get(value[2])
                            elif value[1] == 'text':
                                post[key] = a.text
                response.append(post)
                if post['link'].startswith('http'):
                    if requests.get(post['link']).status_code != 200:
                        continue
                try:
                    post = Post.objects.create(
                        title=post['title'],
                        link=post['link'],
                        imgUrl=post['img'],
                        summary=post['summary'],
                        slug=slugify(post['title']),
                        source=u.source
                    )
                    for t in u.taxonomies.all():
                        post.taxonomies.add(t)
                except:
                    continue
        return response

    def create(self, validated_data):
        raise MethodNotAllowed('Solicitud no permitida')

    def update(self, instance, validated_data):
        raise MethodNotAllowed('Solicitud no permitida')
