
FROM python:3-alpine3.6

ENV PYTHONUNBUFFERED=1

ENV DB_HOST="192.168.1.90"
ENV DB_NAME="feedfancy"
ENV DB_USER="postgres"
ENV DB_PASSWORD="DntOs09273"
ENV DB_PORT="5432"

RUN apk add --no-cache linux-headers bash gcc \
    musl-dev libjpeg-turbo-dev libpng libpq \
    postgresql-dev uwsgi uwsgi-python3 git \
    zlib-dev libmagic

WORKDIR /api
COPY ./ /api
RUN pip install -U -r /api/requirements.txt
CMD python manage.py migrate && uwsgi --ini=/api/uwsgi.ini